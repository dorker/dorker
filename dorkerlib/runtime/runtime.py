"""
Base classes and constants for all runtime types
"""
from dorkerlib.image.image import Image


class BaseRuntime(object):
    """
    Abstract base class of all runtimes
    """
    def __init__(self):
        self.image = None
        self.name = None
        self.layer = None
        self.remove = True

    def setup(self, image, name=None, remove=True):
        """
        Configure this instance
        :param image:
        :param name:
        :param remove: if True, discard the resulting container image
        :return:
        """
        raise NotImplemented()

    def start(self, cmdline, env=None, cwd=None, capture=False):
        """
        Start the instance command
        :param cmdline:
        :param env:
        :param cwd:
        :param capture: if True, return the output
        :return:
        """
        raise NotImplemented()

    def stop(self):
        """
        Stop the instance and possibly delete the disk layer
        :return:
        """
        raise NotImplemented()

    def execute(self, cmdline, env=None, capture=False):
        """
        Run a command in the current instance and wait for it to exit
        :param cmdline:
        :param env:
        :param capture: if True, return the output
        :return:
        """
        raise NotImplemented()

    def save(self, name=None):
        """
        Persist the current disk state and return a new image
        :param name: name to give this state
        :return: new Image
        """
        assert self.layer
        newlayer = self.layer.begin_layer(name=name)
        image = Image(newlayer, self.image.storage)
        return image



