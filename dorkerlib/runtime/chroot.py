"""
Chroot based runtime for Dorker
"""
from dorkerlib.filesystem.base import make_random_string
from dorkerlib.image.image import Image
from dorkerlib.runtime.runtime import BaseRuntime
from dorkerlib.subproc import helpers


class Chroot(BaseRuntime):
    """
    Run an image instance using chroot
    """

    def setup(self, image, name=None, remove=True):
        assert isinstance(image, Image)
        if name is None:
            name = make_random_string(length=16)
        self.image = image
        self.name = name
        self.remove = remove
        self.image.open()
        # make a new fs clone right now, discard it later if remove is True
        self.layer = self.image.filesystem.begin_layer()

    def chroot(self, cmdline, env=None, capture=False):
        """
        Run a command in our layer's chroot
        :param cmdline:
        :param env:
        :return:
        """
        cmd = ["chroot", self.layer.mountpoint]
        cmd.extend(cmdline)
        if capture:
            return helpers.check_output(cmd, cwd="/", env=env, root=True)
        return helpers.check_call(cmd, cwd="/", env=env, root=True)

    def stop(self):
        pass

    def start(self, cmdline, env=None, capture=False):
        assert self.layer
        try:
            return self.chroot(cmdline, env=env, capture=capture)
        finally:
            self.stop()

    def execute(self, cmdline, env=None, capture=False):
        return self.chroot(cmdline, env=env, capture=capture)
