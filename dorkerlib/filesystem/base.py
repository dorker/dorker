"""
Base classes for interaction with filesystems
"""
import random
import string


def make_random_string(length=9):
    """
    Create a random string suitable as a filename
    :return:
    """
    result = ""
    while len(result) < length:
        result += random.choice(string.lowercase + string.digits)
    return result


class BaseFileSystem(object):
    """
    All filesystem implementations stem from here
    """

    def __init__(self):
        self.storage = None
        self.mountpoint = None
        self.is_mounted = False
        self.selected_snapshot = None
        self.child = None
        self.parent = None

    def get_first_parent(self):
        """
        If this filesystem is a child, return the top-most parent
        :return:
        """
        parent = self.parent
        if parent is None:
            return self
        while True:
            if parent.parent is None:
                return parent
            parent = parent.parent

    def get_newest_child(self):
        """
        If this filesystem has children, return the newest one
        :return:
        """
        child = self.child
        if child is None:
            return self
        while True:
            if child.child is None:
                return child
            child = child.child

    def mounted(self):
        """
        Return True if already mounted
        :return:
        """
        return self.is_mounted

    def fstype(self):
        """
        Get the type of filesystem implemenatation we are using
        :return:
        """
        raise NotImplemented()

    def remount(self, mountpoint=None):
        """
        Re-mount this filesystem from last saved settings
        :param mountpoint: change the mount point
        :return:
        """
        raise NotImplemented()

    def mount(self, storage, mountpoint):
        """
        Mount storage at mountpoint
        :param storage:
        :param mountpoint:
        :return:
        """
        raise NotImplemented()

    def umount(self):
        """
        Un-mount this filesystem
        :return:
        """
        raise NotImplemented()

    def can_snapshot(self):
        """
        Return True if we can create a snapshot
        :return:
        """
        raise NotImplemented()

    def can_layer(self):
        """
        Return True if we can create a layer
        :return:
        """
        raise NotImplemented()

    def begin_layer(self, name=None):
        """
        Open a new layer starting now and return it
        :param name: optional name, else random
        :return:
        """
        raise NotImplemented()

    def end_layer(self):
        """
        Close a layer
        :return:
        """
        raise NotImplemented()

    def discard(self):
        """
        Throw this away
        :return:
        """
        raise NotImplemented()

    def get_snapshots(self):
        """
        List all the snapshots
        :return:
        """
        raise NotImplemented()

    def snapshot(self, name):
        """
        Create a snapshot
        :param name: snapshot name
        :return:
        """
        raise NotImplemented()

    def rollback(self, name):
        """
        Roll back to specific snapshot
        :param name:
        :return:
        """
        raise NotImplemented()

    def copy(self, storage, newsize=None):
        """
        Copy the filesystem to new storage
        :param storage:
        :param newsize:
        :return:
        """
        raise NotImplemented()
