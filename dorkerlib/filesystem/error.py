"""
Error types for filesystem
"""


class FilesystemImplError(Exception):
    """
    Some problem occurred when doing a storage operation, could be a bug
    """
    def __init__(self, message):
        super(FilesystemImplError, self).__init__(message)


class FilesystemError(FilesystemImplError):
    """
    We have a problem we cant fix
    """
    def __init__(self, message):
        super(FilesystemError, self).__init__(message)
