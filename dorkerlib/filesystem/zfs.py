"""
ZFS backed filesystem abstractions

The idea here is that we treat zfs clones like "layers". eg:

FROM scratch
- start a new empty pool, make a snapshot "layer"

ADD file.txt
- make a clone from snapshot "layer",
- write file.txt
- make a new snapshot

RUN program args
- make a clone from snapshot "layer"
- run program in mounted chroot/runtime
- make a new snapshot


"""
import os

from dorkerlib.filesystem.base import make_random_string
from dorkerlib.filesystem.error import FilesystemImplError, FilesystemError
from dorkerlib.filesystem.loopback import LoopbackFilesystem, get_device_manager
from dorkerlib.subproc import helpers

POOL_PREFIX = "dorker_storage"
POOL_NAMESPACE = "default"
POOL_FOLDER = "/var/run/dorker/zfs"
DEFAULT_CREATE_SIZE = 2 * 1024 * 1024 * 1024 # 2gb max


class ZfsFactory(object):
    """
    General stuff for zfs
    """
    def __init__(self):
        self.namespace = POOL_NAMESPACE

    def _pool_prefix(self, namespace):
        """
        Get the pool name prefix
        :param namespace:
        :return:
        """
        return "{}-{}-".format(POOL_PREFIX, namespace)

    def create(self, name=None, size=DEFAULT_CREATE_SIZE):
        """
        Make a new zfs backed by a file in POOL_FOLDER
        :param name: optional, if None, generate a new name
        :param size: max number of bytes allowed
        :param namespace: pool namespace suffix
        :return: the new pool name
        """
        if name is None:
            name = make_random_string(length=16)

        loopmanager = get_device_manager()
        if not os.path.exists(POOL_FOLDER):
            os.makedirs(POOL_FOLDER)
        filename = os.path.join(POOL_FOLDER, "{}{}".format(self._pool_prefix(self.namespace), name))

        loopmanager.create_zero_file(filename, size)

        zfs = ZfsLoopbackFilesystem()
        zfs.mountpool(filename, None, force=True)
        return zfs

    def get_pools(self):
        """
        List all the zfs pools
        :return:
        """
        found = []
        prefix = None
        if self.namespace is not None:
            prefix = self._pool_prefix(self.namespace)
        pools = helpers.check_output(["zpool", "list"], root=True)
        for item in pools.splitlines()[1:]:
            name = item.split(" ", 1)[0]
            if not prefix or name.startswith(prefix):
                found.append(name)

        return found

    def clear(self):
        """
        Destroy all dorker zfs items in our namespace
        :return:
        """
        pools = self.get_pools()
        prefix = "{}-{}-".format(POOL_PREFIX, self.namespace)
        for pool in pools:
            if pool.startswith(prefix):
                helpers.check_call(["zpool", "destroy", pool], root=True)

        for filename in os.listdir(POOL_FOLDER):
            if filename.startswith(prefix):
                os.unlink(os.path.join(POOL_FOLDER, filename))


class ZfsFilesystemBase(LoopbackFilesystem):
    """
    Dorker filesystem based on zfs
    """

    def __init__(self):
        super(ZfsFilesystemBase, self).__init__()
        self.pool = None
        self.zfs = None
        self.namespace = "default"
        self.original_mountpoint = None

    def set_namespace(self, namespace):
        """
        Set the zfs pool namespace
        :param namespace:
        :return:
        """
        self.namespace = namespace

    def fstype(self):
        return "zfs"

    def mounted(self):
        return self.is_mounted

    def can_snapshot(self):
        return True

    def can_layer(self):
        return True

    def umount_fs(self):
        """
        Unmount the filesystem only
        :return: 
        """
        if self.mountpoint:
            helpers.check_call(["zfs", "umount", self.zfs], root=True)
            self.mountpoint = None
        self.is_mounted = False

    def check_mounted(self):
        """
        Check if we are mounted or not
        :return:
        """
        mountstate = helpers.check_output(["zfs", "get", "-H", "mounted", self.zfs], root=True)
        return "mounted\tyes" in mountstate

    def remount(self, mountpoint=None):
        self.umount_fs()
        if mountpoint is None:
            mountpoint = self.original_mountpoint

        if self.original_mountpoint is None:
            self.original_mountpoint = mountpoint

        assert mountpoint is not None
        helpers.check_call(["zfs", "set", "mountpoint={}".format(mountpoint), self.zfs], root=True)
        if not self.check_mounted():
            # sometimes setting the mount point will mount the zfs
            helpers.check_call(["zfs", "mount", self.zfs], root=True)
        self.mountpoint = mountpoint
        self.is_mounted = True

    def mountpool(self, filename, mountpoint=None, force=False):
        """
        Mount this file as a pool, the "pool" name is the basename of the file
        :param filename:
        :param mountpoint:
        :return:
        """
        if mountpoint is None:
            mountpoint = "none"
        else:
            if not os.path.exists(mountpoint):
                os.makedirs(mountpoint)

        poolname = os.path.basename(filename)

        cmdline = ["zpool", "create", "-m", mountpoint]
        if force:
            cmdline.append("-f")
        cmdline.extend([poolname, filename])

        helpers.check_call(cmdline, root=True)
        self.pool = poolname
        self.zfs = poolname
        return poolname

    def copy(self, storage, newsize=None):
        """
        Make a zfs copy of this filesystem into a new zfs pool file
        :param storage:
        :param newsize: request that the new backing is at least this many bytes
        :return:
        """
        self.umount_fs()
        newfile = self.loopback_manager.create_zero_file(storage, newsize)
        raise NotImplemented()

    def clone(self, fromsnap, newname):
        """
        Make a zfs clone with the given name, and remount it here, return the clone
        :param fromsnap: snapshot to clone from, eg "pool@snap"
        :param newname:
        :return:
        """
        assert self.can_layer()
        mountpoint = self.original_mountpoint
        self.umount_fs()
        helpers.check_call(
            ["zfs", "clone", "-o", "mountpoint={}".format(mountpoint), fromsnap, newname], root=True)
        return ZfsFilesystemClone(fromsnap, newname, mountpoint)

    def get_snapshots(self):
        listed = helpers.check_output(["zfs", "list", "-r", "-t", "snapshot", self.zfs], root=True)
        found = list()
        for line in listed.splitlines():
            if "@" in line:
                snap = line.split(" ", 1)[0]
                found.append(snap)
        return found

    def snapshot(self, name):
        snapname = "{}@{}".format(self.zfs, name)
        helpers.check_call(["zfs", "snapshot", snapname], root=True)
        return snapname

    def rollback(self, name):
        helpers.check_call(["zfs", "rollback", "-r", "{}@{}".format(self.zfs, name)], root=True)

    def assert_not_mounted(self, storage):
        """
        Complain if file storage is already in use
        :param storage:
        :return:
        """
        raise NotImplemented()

    def begin_layer(self, name=None):
        """
        Make a layer
        :return:
        """
        if name is None:
            name = make_random_string(length=18)
        newname = "{}/{}".format(self.pool, name)
        # snapshot the current fs
        snapname = self.snapshot(name)
        self.child = self.clone(snapname, newname)
        self.child.parent = self

        return self.child


class ZfsFilesystemClone(ZfsFilesystemBase):
    """
    A Cloned zfs filesystem
    """
    def __init__(self, origin, newname, mountpoint):
        super(ZfsFilesystemClone, self).__init__()
        self.pool, self.origin_snap = origin.split("@", 1)
        self.zfs = newname
        self.mountpoint = mountpoint
        self.original_mountpoint = mountpoint
        self.is_mounted = True

    def end_layer(self):
        """
        Close the current layer
        :return:
        """
        self.umount()

    def discard(self):
        """
        Destroy this layer
        :return:
        """
        self.end_layer()
        helpers.check_call(["zfs", "destroy", self.zfs], root=True)

    def assert_not_mounted(self, storage):
        pass

    def mount(self, storage, mountpoint):
        raise NotImplemented()

    def umount(self):
        self.umount_fs()


class ZfsLoopbackFilesystem(ZfsFilesystemBase):
    """
    Dorker filesystem based on zfs on linux
    """

    def get_loopback(self, storage):
        """
        Get the loopback device attached to storage
        :param storage:
        :return:
        """
        return self.loopback_manager.device

    def assert_not_mounted(self, storage):
        self.loopback_manager.assert_not_mounted(storage)

    def umount(self):
        self.umount_fs()

    def mount(self, storage, mountpoint):
        if not os.path.isfile(storage):
            raise FilesystemImplError("{} is not a file".format(storage))
        self.assert_not_mounted(storage)
        try:
            self.mountpool(storage, mountpoint)
            self.mountpoint = mountpoint
            self.original_mountpoint = mountpoint
        except Exception:
            self.umount()
            raise
        self.is_mounted = True
