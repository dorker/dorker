"""
loopback backed filesystems
"""
import os

import sys

from dorkerlib.filesystem.base import BaseFileSystem
from dorkerlib.filesystem.error import FilesystemError
from dorkerlib.subproc import helpers


class LoopDeviceManager(object):
    """
    A Helper class for managing loopback device files
    """
    def __init__(self):
        self.file = None
        self.device = None

    def assert_not_mounted(self, filename):
        """
        Throw an exception if the given file is already in use
        :param filename:
        :return:
        """
        raise NotImplemented()

    def attach(self, filename):
        """
        Attach the file and return the loopback device
        :param filename:
        :return:
        """
        raise NotImplemented()

    def detach(self):
        """
        Disconnect our loopback file
        :return:
        """
        raise NotImplemented()

    def create_zero_file(self, filename, size):
        """
        Create a zeroed file
        :param filename:
        :param size:
        :return:
        """
        raise NotImplemented()


class LinuxLoopDeviceManager(LoopDeviceManager):
    """
    Manage /dev/loop* files on linux
    """

    def create_zero_file(self, filename, size):
        folder = os.path.dirname(filename)
        if not os.path.exists(folder):
            os.makedirs(folder)
        with open(filename, "wb") as newfile:
            newfile.seek(size)
            newfile.write("\0")
        return os.path.abspath(filename)

    def assert_not_mounted(self, filename):
        result = helpers.check_output(["losetup", "-j", filename], root=True)
        if "/dev/loop" in result:
            raise FilesystemError("losetup says '{}' is already in use".format(filename))

    def attach(self, filename):
        self.assert_not_mounted(filename)
        helpers.check_call(["losetup", "-f", filename], root=True)
        result = helpers.check_output(["losetup", "-j", filename], root=True)
        self.file = filename
        self.device = result.split(":", 1)[0]
        return self.device

    def detach(self):
        if self.device:
            helpers.check_call(["losetup", "-d", self.device], root=True)
            self.device = None


def get_device_manager():
    """
    Get a loopback device manager appropriate to this platform
    :return:
    """
    if "linux" in sys.platform:
        return LinuxLoopDeviceManager()
    raise NotImplemented(sys.platform)


class LoopbackFilesystem(BaseFileSystem):
    """
    Filesystems that need to do things with loopback files
    """

    def __init__(self):
        super(LoopbackFilesystem, self).__init__()
        self.loopback_manager = get_device_manager()

    def get_loopback(self, storage):
        """
        Get the loopback device attached to storage
        :param storage:
        :return:
        """
        return self.loopback_manager.device

    def assert_not_mounted(self, storage):
        """
        Throw an error if the file is in use
        :param storage:
        :return:
        """
        self.loopback_manager.assert_not_mounted(storage)
