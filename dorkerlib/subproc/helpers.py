"""
Wrappers around subprocess
"""
import os
import subprocess


def try_sudo():
    """
    Try to run sudo if we aren't root
    :return:
    """
    subprocess.check_output(wrap_sudo(["pwd"]))


def wrap_sudo(cmdline):
    """
    If non root, prefix cmdline with sudo
    :param cmdline:
    :return:
    """
    uid = os.geteuid()
    if uid != 0:
        newcmd = ["sudo", "-n"]
        newcmd.extend(cmdline)
        return newcmd
    return cmdline


def check_call(cmdline, cwd=None, env=None, root=False):
    """
    Wrap subprocess.check_call
    :param cmdline:
    :param cwd:
    :param env:
    :param root: if True, try to use sudo if we need to
    :return:
    """
    if root:
        cmdline = wrap_sudo(cmdline)
    return subprocess.check_call(cmdline, cwd=cwd, env=env)


def check_output(cmdline, cwd=None, env=None, stderr=None, root=False):
    """
    Wrap subprocess.check_output
    :param cmdline:
    :param cwd:
    :param env:
    :param stderr:
    :param root: if True, try to use sudo if we need to
    :return:
    """
    if root:
        cmdline = wrap_sudo(cmdline)
    return subprocess.check_output(cmdline, cwd=cwd, env=env, stderr=stderr)
