"""
Dorker image system
"""
import os

from dorkerlib.filesystem.base import make_random_string
from dorkerlib.subproc import helpers

MOUNT_PREFIX = "/var/dorker"


class Image(object):
    """
    A Dorker image
    """

    def __init__(self, filesys):
        """
        Load an image backed by the given filesystem object
        :param BaseFilesystem filesys: a filesystem implementation
        """
        self.filesystem = filesys
        self.mountpoint = None
        self.instance = None

    def open(self):
        """
        Open an image and return a unique id for it
        :return:
        """
        newid = make_random_string(length=15)
        mountpoint = os.path.join(MOUNT_PREFIX, newid)

        try:
            self.filesystem.remount(mountpoint)
        except Exception:
            self.close()
            raise
        self.mountpoint = mountpoint
        self.instance = newid

    def increment_layer(self):
        """
        Move to a new layer
        :return:
        """
        self.filesystem = self.filesystem.begin_layer()
        self.filesystem.remount()

    def add_file(self, src, dst):
        """
        Add a file to the image
        :param src:
        :param dst:
        :return:
        """
        assert ".." not in dst
        basename = os.path.basename(src)
        self.increment_layer()
        dest = self.mountpoint + dst
        dest = dest.replace("//", "/")
        if os.path.isdir(dest):
            dest = os.path.join(dest, basename)
        else:
            destdir = os.path.dirname(dest)
            if not os.path.exists(destdir):
                helpers.check_call(["mkdir", "-p", destdir], root=True)
        helpers.check_call(["cp", "-f", src, dest], root=True)

    def close(self):
        """
        Stop using this image
        :return:
        """
        self.filesystem.umount()

    def export_tar(self, filename):
        """
        Export this image as it is now, to a tar file
        :param filename:
        :return:
        """
        helpers.check_call(["tar", "-p", "-cf", filename, "-C", self.mountpoint, "."], root=True)

    def import_tar(self, filename):
        """
        Unpack a tar into the filesystem
        :param filename:
        :return:
        """
        self.increment_layer()
        helpers.check_call(["tar", "-S", "-p", "-xf", filename, "-C", self.mountpoint], root=True)
