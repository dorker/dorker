"""
ZFS Filesystem unit tests
"""
import os
import subprocess
import sys
import string
import random
import pytest
from dorkerlib.filesystem import zfs
from dorkerlib.filesystem.zfs import POOL_PREFIX


@pytest.fixture(scope="function")
def zfactory():
    """
    Make a zfs factory
    :return:
    """
    fact = zfs.ZfsFactory()
    fact.namespace = "pytest"
    return fact


def teardown_module():
    """
    Clean up
    :return:
    """
    zfactory().clear()


@pytest.mark.skipif("linux" not in sys.platform, reason="requires linux")
def test_zfs_mount_linux(zfactory, tmpdir):
    """
    Test that on linux we can mount the zero file
    """
    mountpoint = tmpdir.mkdir("mnt").strpath
    fs = zfactory.create()
    fs.remount(mountpoint)

    assert fs.is_mounted


@pytest.mark.skipif("linux" not in sys.platform, reason="requires linux")
def test_zfs_make_layer_linux(zfactory, tmpdir):
    """
    Test we can make some layers
    """
    mountpoint = tmpdir.mkdir("mnt").strpath
    fs = zfactory.create()
    fs.remount(mountpoint)
    assert fs.is_mounted

    textfile = os.path.join(fs.mountpoint, "file.txt")
    textfile2 = os.path.join(fs.mountpoint, "file2.txt")

    # be sure it is empty
    assert not os.path.exists(textfile)
    assert not os.path.exists(textfile2)

    # make the first layer
    first = fs.begin_layer()
    # write a file inside it
    subprocess.check_call(["sudo", "-n", "touch", textfile])
    # which should exist
    assert os.path.exists(textfile)

    # make another layer on top
    second = first.begin_layer()
    # check that our first file still exists
    assert os.path.exists(textfile)
    # make sure the second file doesnt exist yet
    assert not os.path.exists(textfile2)
    # create the second file
    subprocess.check_call(["sudo", "-n", "touch", textfile2])
    # be sure it exists
    assert os.path.exists(textfile2)
    # unmount this layer
    second.end_layer()

    # remount the previous layer
    first.remount()
    # be sure that only the first file exists
    assert os.path.exists(textfile)
    assert not os.path.exists(textfile2)

    # unmount the first layer
    first.end_layer()

    # nothing should exist
    assert not os.path.exists(textfile)
    assert not os.path.exists(textfile2)




