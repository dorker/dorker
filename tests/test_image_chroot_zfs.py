"""
On linux, test out a zfs backed chroot
"""

from test_filesystem_zfs import zfactory, teardown_module
from dorkerlib.image.image import Image
from dorkerlib.runtime.chroot import Chroot


def test_simple_chroot(zfactory):
    """
    Make a chroot
    :param zfactory:
    :return:
    """
    fs = zfactory.create()
    image = Image(fs)
    image.open()
    image.add_file("/bin/busybox", "/bin/busybox")

    chroot = Chroot()
    chroot.setup(image)
    free = chroot.start(["/bin/busybox", "free"], capture=True)
    assert "Mem:" in free

    ls = chroot.start(["/bin/busybox", "ls", "/bin"], capture=True)
    assert "busybox" in ls


def test_export_tar(zfactory):
    """
    Make a chroot and export it as a tar file
    :param zfactory:
    :return:
    """
    fs = zfactory.create()
    image = Image(fs)
    image.open()
    image.add_file("/bin/busybox", "/bin/busybox")
    image.export_tar("/tmp/pytest-fs.tar")

    other = zfactory.create()
    newimage = Image(other)
    newimage.open()
    newimage.import_tar("/tmp/pytest-fs.tar")

    chroot = Chroot()
    chroot.setup(newimage)
    workdir = chroot.start(["/bin/busybox", "pwd"], capture=True).strip()
    assert workdir == "/"
