# Dorker (c) 2018 Ian Norton <inorton@gmail.com>
## What is Dorker

Dorker is like docker, but stupid.  It is intended to be a naive way of managing "images" and running enclosed
environments as if they were docker containers.

Unlike docker it is written in python.

## Why?

Docker is brilliant, I use it myself directly nearly each day. But it doesn't work (yet) on Solaris, AIX or HPUX.

Those platforms do already have mature container technology but they are very different and mostly very complex to setup

Dorker uses one simple thing that these (and linux) have in common. "chroot".

Dorker instances are basically just chroots,
Dorker images are basically just archives of chroot filesystems

# Current Status

It is not ready for any real use yet. At the time of writing it can create zfs backed chroots on linux, add files to
them and run commands inside the chroots.

There are currently no tools, only library and test code.

# Roadmap

Dorker's initial aim is to have a similar experience to docker for the following:

* build - we will probably have an input format similar to a Dockerfile
* pull - we will be able to use archived images stored on a server
* push - we will be able to export local images to a server
* run

It will not support:

* instance IP addresses
* any special network configuration
* anything like docker exec/log/attach


## Initial Supported Platforms

### Linux

Most development is happening here, The linux implementation uses ZFS at the moment for it's backing store

### Solaris

This is the first non-linux target, It uses ZFS for it's backing store

### AIX

This will be the second non-linux target and will probably use LVM for storage.